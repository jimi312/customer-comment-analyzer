package com.ikhokha.techcheck.extractors;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class MoverMentionMetricExtractorTest {
    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsMover() {
        MoverMentionMetricExtractor classUnderTest = new MoverMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("mover");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("MOVER_MENTIONS"));
    }

    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsUpperCaseMover() {
        MoverMentionMetricExtractor classUnderTest = new MoverMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("MOVER");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("MOVER_MENTIONS"));
    }

    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsMixedCaseMover() {
        MoverMentionMetricExtractor classUnderTest = new MoverMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("MoverR");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("MOVER_MENTIONS"));
    }

    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsMoverInSentence() {
        MoverMentionMetricExtractor classUnderTest = new MoverMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("The Mover is awesome");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("MOVER_MENTIONS"));
    }

    @Test public void extractMetrics_ReturnsFalse_WhenLineDoesNotContainMover() {
        MoverMentionMetricExtractor classUnderTest = new MoverMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("Shaker");

        Assert.assertSame(1, testResult.size());
        Assert.assertFalse(testResult.get("MOVER_MENTIONS"));
    }
}
