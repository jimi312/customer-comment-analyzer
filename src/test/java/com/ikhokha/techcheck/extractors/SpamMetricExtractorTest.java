package com.ikhokha.techcheck.extractors;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class SpamMetricExtractorTest {
    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsSpam() {
        SpamMetricExtractor classUnderTest = new SpamMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("Buy some pills from http://pillseller.com");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("SPAM"));
    }

    @Test public void extractMetrics_ReturnsFalse_WhenLineDoesNotContainSpam() {
        SpamMetricExtractor classUnderTest = new SpamMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("This is a legitimate comment!");

        Assert.assertSame(1, testResult.size());
        Assert.assertFalse(testResult.get("SPAM"));
    }
}
