package com.ikhokha.techcheck.extractors;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ShakerMentionMetricExtractorTest {
    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsShaker() {
        ShakerMentionMetricExtractor classUnderTest = new ShakerMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("shaker");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("SHAKER_MENTIONS"));
    }

    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsUpperCaseShaker() {
        ShakerMentionMetricExtractor classUnderTest = new ShakerMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("SHAKER");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("SHAKER_MENTIONS"));
    }

    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsMixedCaseShaker() {
        ShakerMentionMetricExtractor classUnderTest = new ShakerMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("ShakerR");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("SHAKER_MENTIONS"));
    }

    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsShakerInSentence() {
        ShakerMentionMetricExtractor classUnderTest = new ShakerMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("The Shaker is awesome");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("SHAKER_MENTIONS"));
    }

    @Test public void extractMetrics_ReturnsFalse_WhenLineDoesNotContainShaker() {
        ShakerMentionMetricExtractor classUnderTest = new ShakerMentionMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("Mover");

        Assert.assertSame(1, testResult.size());
        Assert.assertFalse(testResult.get("SHAKER_MENTIONS"));
    }
}
