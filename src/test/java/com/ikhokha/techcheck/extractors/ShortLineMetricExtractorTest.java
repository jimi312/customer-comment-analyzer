package com.ikhokha.techcheck.extractors;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ShortLineMetricExtractorTest {
    @Test public void extractMetrics_ReturnsTrue_WhenLineIsShort() {
        ShortLineMetricExtractor classUnderTest = new ShortLineMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("A short line");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("SHORTER_THAN_15"));
    }

    @Test public void extractMetrics_ReturnsFalse_WhenLineIsLong() {
        ShortLineMetricExtractor classUnderTest = new ShortLineMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("A line that is longer than 15 characters");

        Assert.assertSame(1, testResult.size());
        Assert.assertFalse(testResult.get("SHORTER_THAN_15"));
    }
}
