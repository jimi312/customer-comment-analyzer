package com.ikhokha.techcheck.extractors;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class QuestionsMetricExtractorTest {
    @Test
    public void extractMetrics_ReturnsTrue_WhenLineContainsQuestion() {
        QuestionsMetricExtractor classUnderTest = new QuestionsMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("This is a question?");

        Assert.assertSame(1, testResult.size());
        Assert.assertTrue(testResult.get("QUESTIONS"));
    }

    @Test public void extractMetrics_ReturnsFalse_WhenLineDoesNotContainQuestion() {
        QuestionsMetricExtractor classUnderTest = new QuestionsMetricExtractor();
        Map<String, Boolean> testResult = classUnderTest.extractMetrics("This is not a question!");

        Assert.assertSame(1, testResult.size());
        Assert.assertFalse(testResult.get("QUESTIONS"));
    }
}
