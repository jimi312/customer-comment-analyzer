package com.ikhokha.techcheck;

import org.junit.Assert;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CommentAnalyzerTest {
    @Test
    public void analyzeTest() throws IOException {
        LineAnalyzer mockLineAnalyzer = mock(LineAnalyzer.class);
        when(mockLineAnalyzer.analyze(("Line1"))).thenReturn(
                new HashMap<String, Integer>() {{
                    put("MOCK_METRIC", 5);
                    put("MOCK_METRIC_2", 3);
                }}
        );
        when(mockLineAnalyzer.analyze(("Line2"))).thenReturn(
                new HashMap<String, Integer>() {{
                    put("MOCK_METRIC", 1);
                    put("MOCK_METRIC_2", 2);
                    put("MOCK_METRIC_3", 12);
                }}
        );
        when(mockLineAnalyzer.analyze(("Line3"))).thenReturn(
                new HashMap<String, Integer>() {{
                    put("MOCK_METRIC_4", 11);
                    put("MOCK_METRIC_5", 29);
                    put("MOCK_METRIC_6", 2);
                }}
        );
        CommentAnalyzer classUnderTest = new CommentAnalyzer(mockLineAnalyzer);
        String testString = "Line1\nLine2\nLine3";
        Map<String, Integer> testResult = classUnderTest.analyze(new BufferedReader(new StringReader(testString)));
        
        Assert.assertSame(6, testResult.size());
        Assert.assertSame(6, testResult.get("MOCK_METRIC"));
        Assert.assertSame(5, testResult.get("MOCK_METRIC_2"));
        Assert.assertSame(12, testResult.get("MOCK_METRIC_3"));
        Assert.assertSame(11, testResult.get("MOCK_METRIC_4"));
        Assert.assertSame(29, testResult.get("MOCK_METRIC_5"));
        Assert.assertSame(2, testResult.get("MOCK_METRIC_6"));
    }
}
