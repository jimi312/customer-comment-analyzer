package com.ikhokha.techcheck.extractors;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class QuestionsMetricExtractor implements MetricExtractor {
    @Override
    public Map<String, Boolean> extractMetrics(String line) {
        HashMap<String, Boolean> map = new HashMap<>();
        map.put("QUESTIONS", StringUtils.containsIgnoreCase(line, "?"));
        return map;
    }
}
