package com.ikhokha.techcheck.extractors;

import java.util.HashMap;
import java.util.Map;

public class ShortLineMetricExtractor implements MetricExtractor {
    @Override
    public Map<String, Boolean> extractMetrics(String line) {
        HashMap<String, Boolean> map = new HashMap<>();
        map.put("SHORTER_THAN_15", line.length() < 15);
        return map;
    }
}
