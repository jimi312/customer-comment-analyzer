package com.ikhokha.techcheck.extractors;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpamMetricExtractor implements MetricExtractor {

    private final Pattern urlRegex;

    public SpamMetricExtractor() {
        this.urlRegex = Pattern.compile("\\b(?:http|https)://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&@#/%=~_()|]");
    }

    @Override
    public Map<String, Boolean> extractMetrics(String line) {
        HashMap<String, Boolean> map = new HashMap<>();
        map.put("SPAM", containsUrl(line));
        return map;
    }

    private Boolean containsUrl(String line) {
        Matcher matcher = this.urlRegex.matcher(line);
        return matcher.find();
    }
}
