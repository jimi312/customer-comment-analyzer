package com.ikhokha.techcheck.extractors;

import java.util.Map;

public interface MetricExtractor {
    Map<String, Boolean> extractMetrics(String line);
}
