package com.ikhokha.techcheck.extractors;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ShakerMentionMetricExtractor implements MetricExtractor {
    @Override
    public Map<String, Boolean> extractMetrics(String line) {
        HashMap<String, Boolean> map = new HashMap<>();
        map.put("SHAKER_MENTIONS", StringUtils.containsIgnoreCase(line, "Shaker"));
        return map;
    }
}
