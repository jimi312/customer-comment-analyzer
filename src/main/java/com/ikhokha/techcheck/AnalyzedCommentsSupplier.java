package com.ikhokha.techcheck;

import com.ikhokha.techcheck.extractors.*;

import java.io.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

public class AnalyzedCommentsSupplier implements Supplier<Map<String, Integer>> {
    private final File commentFile;

    public AnalyzedCommentsSupplier(File commentFile) {
        this.commentFile = commentFile;
    }

    @Override
    public Map<String, Integer> get() {
        CommentAnalyzer commentAnalyzer = new CommentAnalyzer(new LineAnalyzer(Arrays.asList(
                new ShortLineMetricExtractor(),
                new MoverMentionMetricExtractor(),
                new ShakerMentionMetricExtractor(),
                new QuestionsMetricExtractor(),
                new SpamMetricExtractor()
        )));
        try (BufferedReader reader = new BufferedReader(new FileReader(this.commentFile))) {
            return commentAnalyzer.analyze(reader);
        } catch (FileNotFoundException e) {
            System.out.println("File not found: " + this.commentFile.getAbsolutePath());
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("IO Error processing file: " + this.commentFile.getAbsolutePath());
            e.printStackTrace();
        }
        return new HashMap<>();
    }
}
