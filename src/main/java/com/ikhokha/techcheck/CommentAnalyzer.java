package com.ikhokha.techcheck;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CommentAnalyzer {
    private final LineAnalyzer lineAnalyzer;

    public CommentAnalyzer(LineAnalyzer lineAnalyzer) {
        this.lineAnalyzer = lineAnalyzer;
    }

    public Map<String, Integer> analyze(BufferedReader reader) throws IOException {
        Map<String, Integer> resultsMap = new HashMap<>();
        String line;
        while ((line = reader.readLine()) != null) {
            Map<String, Integer> results = lineAnalyzer.analyze(line);
            incOccurrence(resultsMap, results);
        }
        return resultsMap;
    }

    /**
     * This method increments a counter by 1 for a match type on the countMap. Uninitialized keys will be set to 1
     *
     * @param countMap the map that keeps track of counts
     * @param results  the metrics to increment the total by
     */
    private void incOccurrence(Map<String, Integer> countMap, Map<String, Integer> results) {
        results.forEach((key, count) -> {
            countMap.putIfAbsent(key, 0);
            countMap.put(key, countMap.get(key) + count);
        });
    }

}
