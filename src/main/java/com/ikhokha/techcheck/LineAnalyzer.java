package com.ikhokha.techcheck;

import com.ikhokha.techcheck.extractors.MetricExtractor;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class LineAnalyzer {

    private final List<MetricExtractor> metricExtractors;

    public LineAnalyzer(List<MetricExtractor> metricExtractors) {
        this.metricExtractors = metricExtractors;
    }

    public Map<String, Integer> analyze(String line) {
        return this.metricExtractors
                .stream()
                .map(metricExtractor -> metricExtractor.extractMetrics(line))
                .flatMap(m -> m.entrySet().stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey,
                                Collectors.summingInt(e -> e.getValue() ? 1 : 0)
                        )
                );
    }
}
