package com.ikhokha.techcheck;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        File docPath = new File("docs");
        List<File> commentFiles = Arrays.asList(Objects.requireNonNull(docPath.listFiles((d, n) -> n.endsWith(".txt"))));

        List<CompletableFuture<Map<String, Integer>>> commentAnalyzeFutures = commentFiles
                .stream()
                .map(c -> CompletableFuture.supplyAsync(new AnalyzedCommentsSupplier(c)))
                .collect(Collectors.toList());

        CompletableFuture<Void> allDoneFuture = CompletableFuture.allOf(commentAnalyzeFutures.toArray(new CompletableFuture[0]));

        CompletableFuture<Stream<Map<String, Integer>>> resultFuture = allDoneFuture
                .thenApply(v -> commentAnalyzeFutures
                        .stream()
                        .map(CompletableFuture::join));
        Map<String, Integer> totalResults = resultFuture
                .join()
                .flatMap(m -> m.entrySet().stream())
                .collect(
                        Collectors.groupingBy(
                                Map.Entry::getKey,
                                Collectors.summingInt(Map.Entry::getValue)
                        )
                );

        System.out.println("RESULTS\n=======");
        totalResults.forEach((k, v) -> System.out.println(k + " : " + v));
    }
}
