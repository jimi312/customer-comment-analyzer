## Assessment notes

### Bugs

There were three primary bugs in the initial code that I noticed
1. The structure of the if..else if statements meant that only one would match
2. Matching mover or shaker was case-sensitive
3. Only the last files results were displayed

All these bugs have been resolved.

### Object orientated design

I've changed the structure of the application to use something I've called a MetricExtractor
This is an interface that LineAnalyzer can call to determine if a particular metric has been matched for a particular line
The LineAnalyzer class was added primarily to segregate responsibility a little bit more and make unit testing a little easier
To add new metrics to the application a new MetricExtractor must be implemented and then passed to the CommentAnalyzer constructor in CommentAnalyzerSupplier

This design allowed unit testing most of the application - it's possible to increase the coverage more but I felt the coverage was sufficient for the purposes of the assessment

### Concurrency

I elected to use CompletableFutures to implement concurrency. This has a number of benefits

1. No thread management. CompletableFutures are run on thread pools which significantly simplifies the code
2. No thread exhaustion. Since the only threads used are in a thread pool there is no risk of accidentally creating too many threads.
   If more control over the exact number of threads used is required an Executor can be passed to supplyAsync
   
### Unit tests

In addition I added unit tests to the project to ensure that the code does the right thing. The unit tests are not exhaustive but they cover a reasonable amount of the code

### Matching accuracy

Something worth noting is the logic for matching mover, shaker and URLs is not bulletproof

The check for mover or shaker is a simple cas insensitve contains. That means that words like remover will match.
Using a regex like \bmover\b would solve that but introduce other issue - such as no longer matching movers.
Using contains gave better results on this dataset which is why I went with.

Matching URLs is a error prone business. The regex used here will match most URLs that start with http/https. It will probably miss a few.
In order to really determine if a URL is valid a request has to be made to it.
However since 'http://www.myrandomsite.co.za' is a URL in one of the comments and it doesn't actually exist I haven't implemented that